package de.onlineferries.servicesimpl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.UserTransaction;

import de.onlineferries.entities.Cabin;
import de.onlineferries.entities.CabinReservation;
import de.onlineferries.entities.Customer;
import de.onlineferries.entities.Reservation;
import de.onlineferries.entities.Route;
import de.onlineferries.entities.Travellers;
import de.onlineferries.entities.Trip;
import de.onlineferries.managedbeans.ServiceFactory;
import de.onlineferries.services.ReservationService;
import de.onlineferries.view.CustomerView;
import de.onlineferries.view.ReservationTableView;
import de.onlineferries.view.ReservationView;
import de.onlineferries.view.RouteView;
import de.onlineferries.view.ShipCabinView;
import de.onlineferries.view.TravellerView;
import de.onlineferries.view.TripView;

@Named
@RequestScoped
public class ReservationServiceImpl implements Serializable, ReservationService {

	private static final long serialVersionUID = 1L;

	@PersistenceContext
	private EntityManager em;
	
	@Resource
	private UserTransaction ut;
	
	@Inject
	private ServiceFactory serviceFactory;

	
	@Override
	public double getReservationPrice(int trip_id, List<ShipCabinView> shipCabins, int cars, int travellers) {

		double reservation_price = 0;
		
		try {
			
			Trip trip = em.find(Trip.class,  trip_id);
			reservation_price += cars * trip.getPrice_car();
			reservation_price += (travellers+1) * trip.getPrice_passenger();
			
			for (ShipCabinView shipCabin : shipCabins) {
				if (shipCabin.getRes_count() > 0) {
					reservation_price += shipCabin.getRes_count() * shipCabin.getPrice();
				}
			}
			
		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		return reservation_price;
	}

	@Override
	public void insertReservation(ReservationView reservationView) {
		
		try {
			
			Reservation reservation = new Reservation();
			
			CustomerView customerView = reservationView.getCustomer();
			Customer customer = null;
			if (customerView.getCustomer_id() != 0) {
				customer = em.find(Customer.class, customerView.getCustomer_id());
				customer.getReservations().add(reservation);
			}
			else {
				// kann jetzt noch nicht vorkommen
				customer = new Customer();
			}
			reservation.setCustomer(customer);
			
			Trip trip = em.find(Trip.class, reservationView.getTrip().getTrip_id());
			reservation.setTrip(trip);
			
			reservation.setCars(reservationView.getCars());

			// Speichern von Mitreisenden
			if (reservationView.getTravellerNames() != null) {
				List<Travellers> tr = new ArrayList<Travellers>();
				for (int i = 0; i < reservationView.getTravellerNames().size(); i++) {
					Travellers traveller = new Travellers(reservationView.getTravellerNames().get(i).getName());
					traveller.setReservation(reservation);
					tr.add(traveller);
				}
				reservation.setTravellers(tr);
			}


			// Speichern der Kabinen
			if (reservationView.getShipCabins() != null) {
				List<CabinReservation> res_cabins = new ArrayList<CabinReservation>();
				for (int i = 0; i < reservationView.getShipCabins().size(); i++) {
					ShipCabinView shipCabinView = reservationView.getShipCabins().get(i);
					if (shipCabinView.getRes_count() > 0) {
						CabinReservation cabinReservation = new CabinReservation();
						Cabin cabin = em.find(Cabin.class, shipCabinView.getCabin_id());
						cabinReservation.setCabin(cabin);
						cabinReservation.setCount(shipCabinView.getRes_count());
						cabinReservation.setReservation(reservation);
						res_cabins.add(cabinReservation);
						System.out.println("***** Kabine hinzugefügt: " +cabin.getId());
					}
				}
				reservation.setCabins(res_cabins);

			}
			
			ut.begin();
			em.persist(reservation);
			ut.commit();
			
		}
		catch (Exception ex) { 
			ex.printStackTrace(); 
		}
		
		
		
	}

	@Override
	public List<ReservationTableView> getReservations(int customer_id) {
		List<ReservationTableView> reservationViews = null;
		
		try {
			// Get all Reservations
			TypedQuery<Reservation> query= em.createQuery("from de.onlineferries.entities.Reservation r where r.customer.id = " + customer_id, Reservation.class);
			List<Reservation> reservations = query.getResultList();
			
			// Get ReservationsViews
			reservationViews = new ArrayList<ReservationTableView>();
			for (Reservation reservation: reservations) {
				reservationViews.add(ReservationTableView.fromReservation(reservation));
			}
		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		return reservationViews;
	}
	
	@Override
	public void editReservation(ReservationView reservationView) {
		
		try {
			Reservation reservation = em.find(Reservation.class, reservationView.getReservation_id());
			
			//reservation.setId(reservationView.getReservation_id());
			
			CustomerView customerView = reservationView.getCustomer();
			Customer customer = null;
			if (customerView.getCustomer_id() != 0) {
				customer = em.find(Customer.class, customerView.getCustomer_id());
				customer.getReservations().add(reservation);
			}
			else {
				// kann jetzt noch nicht vorkommen
				customer = new Customer();
			}
			reservation.setCustomer(customer);
			
			Trip trip = em.find(Trip.class, reservationView.getTrip().getTrip_id());
			reservation.setTrip(trip);
			
			reservation.setCars(reservationView.getCars());

			// Speichern von Mitreisenden
			System.out.println("Saving Travellers: ");
			if (reservationView.getTravellerNames() != null) {
				List<Travellers> tr = new ArrayList<Travellers>();
				for (int i = 0; i < reservationView.getTravellerNames().size(); i++) {
					Travellers traveller = new Travellers(reservationView.getTravellerNames().get(i).getName());
					System.out.println(traveller.getFullName());
					traveller.setReservation(reservation);
					tr.add(traveller);
				}
				reservation.setTravellers(tr);
			} else {
				reservation.setTravellers(null);
			}


			// Speichern der Kabinen
			if (reservationView.getShipCabins() != null) {
				List<CabinReservation> res_cabins = new ArrayList<CabinReservation>();
				for (int i = 0; i < reservationView.getShipCabins().size(); i++) {
					ShipCabinView shipCabinView = reservationView.getShipCabins().get(i);
					if (shipCabinView.getRes_count() > 0) {
						CabinReservation cabinReservation = new CabinReservation();
						Cabin cabin = em.find(Cabin.class, shipCabinView.getCabin_id());
						cabinReservation.setCabin(cabin);
						cabinReservation.setCount(shipCabinView.getRes_count());
						cabinReservation.setReservation(reservation);
						res_cabins.add(cabinReservation);
						System.out.println("***** Kabine hinzugefügt: " +cabin.getId());
					}
				}
				reservation.setCabins(res_cabins);

			}

			
			ut.begin();
			em.merge(reservation);
			ut.commit();
			
		}
		catch (Exception ex) { 
			ex.printStackTrace(); 
		}
		
		
		
	}

	@Override
	public ReservationView getReservation(int reservation_id) {
		ReservationView reservationView = null;
		try {
			Reservation reservation = em.find(Reservation.class,  reservation_id);
			
			TripView tripView = TripView.fromTrip(reservation.getTrip());
			
			CustomerView customerView = CustomerView.fromCustomer(reservation.getCustomer());
			
			List<ShipCabinView> shipCabins = serviceFactory.getShipService().findAllShipCabins(reservation.getTrip().getRoute().getShip().getId());
			
			for(CabinReservation cr : reservation.getCabins()) {
				for(ShipCabinView scv : shipCabins) {
					if(cr.getCabin().getId() == scv.getCabin_id()) {
						scv.setRes_count(cr.getCount());
					}
				}
			}
			
			int cars = reservation.getCars();
			
			List<TravellerView> travellers = new ArrayList<>();
			for(Travellers t : reservation.getTravellers()) {
				travellers.add(TravellerView.fromTraveller(t));
			}
			
			reservationView = new ReservationView(tripView, customerView, shipCabins, cars, travellers);
			reservationView.setReservation_id(reservation_id);
			
		} catch(Exception e) {
			e.printStackTrace();
		}

		
		return reservationView;
	}
	
}
