package de.onlineferries.servicesimpl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import de.onlineferries.entities.Route;
import de.onlineferries.entities.Ship;
import de.onlineferries.entities.ShipCabin;
import de.onlineferries.services.ShipService;
import de.onlineferries.view.ShipCabinView;
import de.onlineferries.view.ShipView;

@Named
@RequestScoped
public class ShipServiceImpl  implements Serializable, ShipService {

	private static final long serialVersionUID = 1L;

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public ShipView findShip(int route_id) {
		
		try {
			Route route = em.find(Route.class,  route_id);
			Ship ship = route.getShip();
			
			return new ShipView(ship.getId(), ship.getDescription(), ship.getCars(), ship.getPassengers());
		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		return null;
	}

	@Override
	public List<ShipCabinView> findAllShipCabins(int ship_id) {
				
		try {
		
			Ship ship = em.find(Ship.class, ship_id);
			List<ShipCabin> shipCabins = ship.getShipCabin();
			
			List<ShipCabinView> shipCabinViews = new ArrayList<>();
			
			for (ShipCabin shipCabin: shipCabins) {
				shipCabinViews.add(new ShipCabinView(
						shipCabin.getCabin().getId(),
						shipCabin.getCabin().getDescription(),
						shipCabin.getCabin().getPassengers(),
						shipCabin.getCount(),
						0,
						shipCabin.getPrice()));
			}
			return shipCabinViews;
		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		return null;
	}

}
