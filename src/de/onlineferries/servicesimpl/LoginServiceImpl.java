package de.onlineferries.servicesimpl;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import de.onlineferries.entities.Customer;
import de.onlineferries.services.LoginService;
import de.onlineferries.view.CustomerView;

@Named
@RequestScoped
public class LoginServiceImpl implements LoginService, Serializable {

	private static final long serialVersionUID = 1L;
	
	@PersistenceContext
	private EntityManager em;
	
	@Resource
	private UserTransaction ut;
	
	@Override
	public CustomerView login(String username, String password) {
			
		CustomerView customerView = null;
		
		try {
			Query q = em.createNamedQuery("loginCustomer", Customer.class);
			q.setParameter("username", username);
			q.setParameter("password", password);
			Customer customer = (Customer)q.getSingleResult();
			customerView = new CustomerView(
					customer.getId(), 
					customer.getEmail(), 
					customer.getPassword(), 
					customer.getName(), 
					customer.getFirstname());
			customerView.setStreet(customer.getStreet());
			customerView.setZip(customer.getZipcode());
			customerView.setCity(customer.getCity());
			customerView.setIban(customer.getIban());
		}
		catch(NoResultException ex) {}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return customerView;
	}

	@Override
	public Integer saveCustomer(CustomerView customerView) {
		
		try {
			
			Customer customer = new Customer();
			customer.setEmail(customerView.getEmail());
			customer.setPassword(customerView.getPassword());
			customer.setName(customerView.getLastname());
			customer.setFirstname(customerView.getFirstname());
			customer.setStreet(customerView.getStreet());
			customer.setZipcode(customerView.getZip());
			customer.setCity(customerView.getCity());
			customer.setIban(customerView.getIban());
			
			ut.begin();
			em.persist(customer);
			ut.commit();
			System.out.println("Customer gespeichert: " + customer.getId());
			
			return customer.getId();
			
		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		return null;
	}
	
	@Override
	public Integer editCustomer(CustomerView customerView) {
		
		try {
			
			Customer customer = new Customer();
			customer.setEmail(customerView.getEmail());
			customer.setPassword(customerView.getPassword());
			customer.setName(customerView.getLastname());
			customer.setFirstname(customerView.getFirstname());
			customer.setStreet(customerView.getStreet());
			customer.setZipcode(customerView.getZip());
			customer.setCity(customerView.getCity());
			customer.setIban(customerView.getIban());
			customer.setId(customerView.getCustomer_id());
			
			ut.begin();
			em.merge(customer);
			ut.commit();
			System.out.println("Customer gespeichert: " + customer.getId());
			
			return customer.getId();
			
		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		return null;
	}
}
