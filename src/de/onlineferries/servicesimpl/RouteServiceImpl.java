package de.onlineferries.servicesimpl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import de.onlineferries.entities.Route;
import de.onlineferries.entities.Trip;
import de.onlineferries.services.RouteService;
import de.onlineferries.view.RouteView;
import de.onlineferries.view.TripView;

@Named
@RequestScoped
public class RouteServiceImpl implements Serializable, RouteService {

	private static final long serialVersionUID = 1L;
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<RouteView> findAllRoutes() {
		
		List<RouteView> routeViews = null;
				
		try {
			TypedQuery<Route> query= em.createQuery("from de.onlineferries.entities.Route r", Route.class);
			List<Route> routes = query.getResultList();
			routeViews = new ArrayList<RouteView>();
			for (Route route: routes) {
				routeViews.add(new RouteView(route.getId(), route.getStart(), route.getDestination()));
			}
		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		return routeViews;
	}

	@Override
	public List<TripView> findTrips(int route_id) {
		
		List<TripView> tripViews = null;
		
		try {
			Route route = em.find(Route.class, route_id);
			List<Trip> trips = route.getTrips();
			tripViews = new ArrayList<TripView>();
			for (int i = 0; i < trips.size(); i++) {
				Trip trip = trips.get(i);
				tripViews.add(new TripView(
					trip.getId(), 
					trip.getDate(), 
					trip.getDeparture(), 
					trip.getArrival(), 
					route_id, 
					trip.getPrice_car(), 
					trip.getPrice_passenger()));
			}			
		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		return tripViews;

	}

	@Override
	public RouteView findRoute(int route_id) {
		RouteView view = null;
		try {
			view = RouteView.fromRoute(em.find(Route.class, route_id));
		} catch(Exception e) {
			e.printStackTrace();
		}
		return view;
	}
	
	
}
