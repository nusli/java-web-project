package de.onlineferries.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

import de.onlineferries.services.ReservationService;
import de.onlineferries.view.ReservationTableView;
import de.onlineferries.view.ReservationView;
import de.onlineferries.view.ShipCabinView;
import de.onlineferries.view.TravellerView;

@Named
@SessionScoped
public class ReservationTableHandler implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ServiceFactory serviceFactory;
	
	@Inject
	private LoginHandler loginHandler;

	private List<ReservationTableView> reservationTableViews;
	
	public ReservationTableHandler() {
		reservationTableViews = new ArrayList<>();
	}
	
	public String reservationsInfo() {
		System.out.println("Get Reservations...");
		reservationTableViews = serviceFactory.getReservationService().getReservations(loginHandler.getCustomer().getCustomer_id());
		
		return "success";
	}

	public List<ReservationTableView> getReservationTableViews() {
		return reservationTableViews;
	}

	public void setReservationTableViews(List<ReservationTableView> reservationTableViews) {
		this.reservationTableViews = reservationTableViews;
	}
	
	
}
