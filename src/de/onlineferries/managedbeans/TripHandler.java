package de.onlineferries.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import de.onlineferries.entities.CabinReservation;
import de.onlineferries.entities.Reservation;
import de.onlineferries.entities.Route;
import de.onlineferries.entities.ShipCabin;
import de.onlineferries.view.RouteView;
import de.onlineferries.view.ShipCabinView;
import de.onlineferries.view.ShipView;
import de.onlineferries.view.TripView;

@Named
@SessionScoped
public class TripHandler implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ServiceFactory serviceFactory;
	
	@Inject
	private RouteHandler routeHandler;
	
	private List<TripView> trips;
	public List<TripView> getTrips() { return trips; }
	
	@PersistenceContext
	private EntityManager em;
	
	private TripView trip;
	public TripView getTrip() { return trip; }
	public void setTrip(TripView trip) { this.trip = trip; }

	public String tripsInfo() {
		
		trips = serviceFactory.getRouteService().findTrips(routeHandler.getRoute().getRoute_id());
		if (trips.size() > 0)
			trip = trips.get(0);
		
		return "success";
	}
	
	public String back() {
		return "/selectRoute.xhtml?faces-redirect=true";
	}
	
	public Converter<TripView> getTripConverter() {
		return new Converter<TripView>() {

			@Override
			public TripView getAsObject(FacesContext context, UIComponent component,
					String value) {
				for (int i = 0; i < trips.size(); i++) {
					TripView t = trips.get(i);
					if ((new Integer(t.getTrip_id()).toString()).equals(value))
						return t;
				}				
				return null;
			}

			@Override
			public String getAsString(FacesContext context, UIComponent component,
					TripView value) {
				return (new Integer(value.getTrip_id())).toString();
			}			
		};
	}
	
	/* Aufgabe 3
	 * Reservierte Autopl�tze ermitteln
	 */
	public long getCars() {
		long cars = 0;
		
		
		try {
			/* older version
			TypedQuery<Reservation> query= em.createQuery("select r from de.onlineferries.entities.Reservation r where r.trip.id = :trip_id", Reservation.class).setParameter("trip_id",  trip.getTrip_id());
			List<Reservation> res = query.getResultList();
			cars = res.size();
			for (Reservation r: res) {
				cars += r.getCars();
			}
			//System.out.println(cars); */
			Query query = em.createQuery(
					"select Sum(r.cars) from de.onlineferries.entities.Reservation r where r.trip.id = :trip_id", Reservation.class).setParameter("trip_id",  trip.getTrip_id());

			Object obj = query.getSingleResult();
			if (obj != null) {
				cars = (long) obj;
			}
			
			//System.out.println("***\n***\n***" + obj + " Object\n***\n***\n***");

		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		System.out.println("***\n***\n***" + cars + " Autos gefunden\n***\n***\n***");
		return cars;
	}
	
	public long getCarsRemaining(ShipView ship) {
		long remaining = ship.getCars() - getCars();
		
		if (remaining < 0) return 0;
		return remaining;
	}
	
	/*
	 * Aufgabe 3
	 * Passagiere ermitteln
	 */
	public long getPassengers() {
		long passengers = 0;
		
		
		try {
			Query queryR = em.createQuery("select r.id from de.onlineferries.entities.Reservation r");
			List<Object> reservations = queryR.getResultList();
			for (Object id: reservations) {
				Query queryP = em.createQuery(
						"select Count(t) from de.onlineferries.entities.Travellers t where t.reservation.id = :reservation_id AND t.reservation.trip.id = :trip_id")
						.setParameter("reservation_id", id).setParameter("trip_id",  trip.getTrip_id());

				Object obj = queryP.getSingleResult();
				if (obj != null) {
					passengers += (long) obj;	
				}
					 
					 
			}
			
			
			
				 //System.out.println("***\n***\n***" + obj + " Object\n***\n***\n***");

		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		System.out.println("***\n***\n***" + passengers + " Passagiere gefunden\n***\n***\n***");
		return passengers;
	}
	
	public long getPassengersRemaining(ShipView ship) {
		long remaining = ship.getPassengers() - getPassengers();
		
		if (remaining < 0) return 0;
		return remaining;
	}
	
	public long getCabinsRes(int cabin_id) {
		long cabinsRes = 0;
		
		/*
		try {
			Query queryR = em.createQuery("select r.id from de.onlineferries.entities.Reservation r");
			List<Object> reservations = queryR.getResultList();
			for (Object id: reservations) {
				Query queryP = em.createQuery(
						"select Count(t) from de.onlineferries.entities.Travellers t where t.reservation.id = :reservation_id AND t.reservation.trip.id = :trip_id")
						.setParameter("reservation_id", id).setParameter("trip_id",  trip.getTrip_id());

					 Object obj = queryP.getSingleResult();
					 cabinsRes += (long) obj;
			}
			
			
			
				 //System.out.println("***\n***\n***" + obj + " Object\n***\n***\n***");

		}
		catch (Exception ex) { ex.printStackTrace(); }
		*/
		
		try {
			Query queryC = em.createQuery(
					"select r.cabins from de.onlineferries.entities.Reservation r" + 
			" where r.trip.id = :trip_id")
					.setParameter("trip_id",  trip.getTrip_id());
			
			List<CabinReservation> cabins = queryC.getResultList();
			//System.out.println("***\n***\ncabins: " + cabins + "\n***\n***\n");
			if (cabins != null) {
				//System.out.println("***\n***\nL�nge: " + cabins.size() + "\n***\n***\n");
				for (CabinReservation cr : cabins) {
					if (cr == null) continue;
					 if (cr.getCabin().getId() == cabin_id) {
						 cabinsRes += cr.getCount();
					 }
				}
			}
			
				 
		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		System.out.println("***\n***\n***" + cabinsRes + " Kabinen gefunden " + cabin_id + "\n***\n***\n***");
		return cabinsRes;
	}
	
	public long getCabinsRemaining (ShipCabinView sc) {
		long remaining = sc.getCount() - getCabinsRes(sc.getCabin_id());
		
		if (remaining < 0) return 0;
		return remaining;
		
		
	}
}
