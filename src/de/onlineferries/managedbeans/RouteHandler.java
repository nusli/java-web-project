package de.onlineferries.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import de.onlineferries.view.RouteView;
import de.onlineferries.view.ShipView;

@Named
@SessionScoped
public class RouteHandler  implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ServiceFactory serviceFactory;
	
	private List<RouteView> routes;
	public List<RouteView> getRoutes() { return routes; }
	
	private RouteView route;
	public RouteView getRoute() { return route; }
	public void setRoute(RouteView route) { this.route = route; }
	
	private ShipView ship;
	public ShipView getShip() { return ship; }
	
	public String routesInfo() {
		
		routes = serviceFactory.getRouteService().findAllRoutes();
		if (routes.size() > 0)
			route = routes.get(0);
		ship = serviceFactory.getShipService().findShip(route.getRoute_id());
		
		return "success";
	}
	
	public String back() {
		return "back";
	}
	
	public Converter<RouteView> getRouteConverter() {
		return new Converter<RouteView>() {

			@Override
			public RouteView getAsObject(FacesContext context, UIComponent component,
					String value) {
				for (int i = 0; i < routes.size(); i++) {
					RouteView r = routes.get(i);
					if ((new Integer(r.getRoute_id()).toString()).equals(value))
						return r;
				}				
				return null;
			}

			@Override
			public String getAsString(FacesContext context, UIComponent component,
					RouteView value) {
				return (new Integer(value.getRoute_id())).toString();
			}			
		};
	}
	
	public void routeChanged(ValueChangeEvent vce) {
		
		try {
			RouteView route = (RouteView)vce.getNewValue();
			ship = serviceFactory.getShipService().findShip(route.getRoute_id());
		}
		catch (Exception ex) { ex.printStackTrace(); }
	}
	
	public void setShip(ShipView ship) {
		this.ship = ship;
	}

	
}
