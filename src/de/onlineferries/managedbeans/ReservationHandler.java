package de.onlineferries.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

import de.onlineferries.services.ReservationService;
import de.onlineferries.view.ReservationTableView;
import de.onlineferries.view.ReservationView;
import de.onlineferries.view.ShipCabinView;
import de.onlineferries.view.TravellerView;

@Named
@SessionScoped
public class ReservationHandler implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ServiceFactory serviceFactory;
	
	@Inject
	private RouteHandler routeHandler;
	
	@Inject
	private TripHandler tripHandler;
	
	@Inject
	private LoginHandler loginHandler;
	
	@Inject LocaleHandler localeHandler;
	@Inject
	private ReservationTableHandler reservationTableHandler;

	private List<ShipCabinView> shipCabins;
	private int cars;
	private int travellers;
	private List<TravellerView> travellerNames;
	private double reservationPrice;
	
	private int editReservation_id;
	
	public int[] getTravellerValues() { return new int[] { 0, 1, 2, 3, 4, 5, 6 }; }
	
	public List<ShipCabinView> getShipCabins() {
		return shipCabins;
	}
	public void setShipCabins(List<ShipCabinView> shipCabins) {
		this.shipCabins = shipCabins;
	}

	public int getCars() {
		return cars;
	}
	public void setCars(int cars) {
		this.cars = cars;
	}

	public int getTravellers() {
		return travellers;
	}
	public void setTravellers(int travellers) {
		this.travellers = travellers;
	}

	public List<TravellerView> getTravellerNames() {
		return travellerNames;
	}
	public void setTravellerNames(List<TravellerView> travellerNames) {
		this.travellerNames = travellerNames;
	}
	
	public double getReservationPrice() {
		return reservationPrice;
	}
	
	public int getEditReservation_id() {
		return editReservation_id;
	}

	public void setEditReservation_id(int editReservation_id) {
		this.editReservation_id = editReservation_id;
	}

	public List<ShipCabinView> getSelectedShipCabins() {
		
		List<ShipCabinView> selectedShipCabins = new ArrayList<ShipCabinView>();
		
		for (ShipCabinView sc : shipCabins) {
			if (sc.getRes_count() > 0)
				selectedShipCabins.add(sc);
		}
		
		return selectedShipCabins;
	}

	public String enterReservation() {
		
		shipCabins = serviceFactory.
				getShipService().findAllShipCabins(routeHandler.getShip().getShip_id());
		return "success";
	}
	
	public String selectCustomerType() {
		ResourceBundle rb = ResourceBundle.getBundle("messages", new Locale( localeHandler.getLocale()));

		/* validate input */
		boolean check = false;
		String errMsg = "";
		// cabins
		for (ShipCabinView cabin : shipCabins) {
			long remaining = tripHandler.getCabinsRemaining(cabin);
			//long remaining = cabin.getCount() - tripHandler.getCabinsRes(cabin.getCabin_id());
			if (cabin.getRes_count() > remaining) {
				check = true;
				errMsg += rb.getString("onlineferries_cabin_err1") + cabin.getCabinDescr();
				errMsg += rb.getString("onlineferries_cabin_err2") + remaining;
				errMsg += rb.getString("onlineferries_cabin_err3") + "\n";
				//errMsg += "ungültige Anzahl von Kabinen (" + cabin.getCabinDescr() + " max: " + remaining + ")\n";
			}
		}
		// cars
		long remainingc = tripHandler.getCarsRemaining(routeHandler.getShip());
		//long remainingc = routeHandler.getShip().getCars() - tripHandler.getCars();
		if (getCars() > remainingc) {
			check = true;
			errMsg += rb.getString("onlineferries_car_err1") + remainingc;
			errMsg += rb.getString("onlineferries_car_err2") + "\n";
			//errMsg += "ungültige Anzahl von Autos ( max: " + remainingc + ")\n";
		}
		// passengers
		long remainingp = tripHandler.getPassengersRemaining(routeHandler.getShip());
		//long remainingp = routeHandler.getShip().getPassengers() - tripHandler.getPassengers();
		if (getTravellers() > remainingp) {
			check = true;
			errMsg += rb.getString("onlineferries_passenger_err1") + remainingp;
			errMsg += rb.getString("onlineferries_passenger_err2") + "\n";
			//errMsg += "ungültige Anzahl von Passagieren ( max: " + remainingp + ")\n";
		}
			
		if (check) {
			FacesMessage fm = new FacesMessage(errMsg);
		    FacesContext.getCurrentInstance().addMessage("reservation", fm);
		    return "back";
		}
		
		
		ReservationService reservationService = serviceFactory.getReservationService();
		reservationPrice = reservationService.getReservationPrice(tripHandler.getTrip().getTrip_id(), shipCabins, getCars(), getTravellers());

		if (loginHandler.getCustomer() != null && loginHandler.getCustomer().getCustomer_id() > 0)	// bereits angemeldet
			return "success";
		
		return "successSelect";
	}

public String saveReservation() {
		
		ReservationView reservationView = new ReservationView(
				tripHandler.getTrip(),
				loginHandler.getCustomer(),
				shipCabins,
				cars,
				travellerNames);
		serviceFactory.getReservationService().insertReservation(reservationView);
		
		return "successSave";
	}

	public String back() {
		return "/selectTrip.xhtml?faces-redirect=true";
	}
	
	public String back2() {
		return "/enterReservation.xhtml?faces-redirect=true";
	}

	public void changeTraveller(ValueChangeEvent ev) {	
		int previousT = travellers;
		travellers = (Integer)ev.getNewValue();
		if(travellers > previousT) {
			if (travellerNames == null) {
				travellerNames = new ArrayList<TravellerView>(travellers);
			}
			for (int i = travellerNames.size()-1; i < travellers-1; i++) travellerNames.add(new TravellerView(i, ""));
		} else if(travellers < previousT) {
			int deleteAmount = previousT - travellers;
			for(int i = 0; i < deleteAmount; i++) {
				travellerNames.remove(travellerNames.size()-1);
			}
		}
		
		FacesContext.getCurrentInstance().renderResponse();
	}

	public void validateTravellerName(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
			
	}
	
	public void test() {
		System.out.println("TEST TEST TEST");
	}
	
	public String loadReservation(Integer reservation_id) {
		System.out.println("Loading Reservation...");
		
		ReservationView reservationView = serviceFactory.getReservationService().getReservation(reservation_id);
		
		routeHandler.setRoute(serviceFactory.getRouteService().findRoute(reservationView.getTrip().getRouteid()));
		routeHandler.setShip(serviceFactory.getShipService().findShip(reservationView.getTrip().getRouteid()));
		tripHandler.setTrip(reservationView.getTrip());
		shipCabins = reservationView.getShipCabins();
		cars = reservationView.getCars();
		travellerNames = reservationView.getTravellerNames();
		
		setEditReservation_id(reservation_id);
		
		return "successLoad";
	}
	
	public String editReservation() {
		ReservationView reservationView = new ReservationView(
				tripHandler.getTrip(),
				loginHandler.getCustomer(),
				shipCabins,
				cars,
				travellerNames);
		reservationView.setReservation_id(getEditReservation_id());
		setEditReservation_id(0);
		serviceFactory.getReservationService().editReservation(reservationView);
		
		reservationTableHandler.reservationsInfo();
		
		return "successEdit";
	}
	
	public double getTotalPrice() {
		double amount = 0;
		
		for(ShipCabinView cabin : shipCabins) {
			amount += cabin.getPrice() * cabin.getRes_count();
		}
		
		amount += cars * tripHandler.getTrip().getCarPrice();
		
		amount += (travellerNames.size()+1) * tripHandler.getTrip().getPassengerPrice();
		
		return amount;
	}
	
	public String backToReservationList() {
		return "/showCustomerReservations.xhtml?faces-redirect=true";
	}
	
}

