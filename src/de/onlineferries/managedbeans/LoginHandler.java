package de.onlineferries.managedbeans;

import java.io.Serializable;
import java.util.ResourceBundle;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import de.onlineferries.view.CustomerView;

@Named
@SessionScoped
public class LoginHandler implements Serializable {

	private static final long serialVersionUID = 1L;

	private String username;
	private String password;
	private CustomerView customer;

	private String displayName;
	
//	@Inject
//	private LoginService loginService;
	
	@Inject
	private ServiceFactory serviceFactory;
	
	public LoginHandler() {
		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle rb = context.getApplication().getResourceBundle(context, "msg");	
		displayName = rb.getString("onlineferries_login");
	}
	
	public String login() {
		
//		customer = loginService.login(username, password);
		customer = serviceFactory.getLoginService().login(username, password);
		if (customer == null) {
			System.out.println("keim Customer gefunden");
			return "retry";
		}
		System.out.println("Customer gefunden");
		
		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle rb = context.getApplication().getResourceBundle(context, "msg");	
		displayName = rb.getString("onlineferries_logout") + "(" + customer.getLastname() + ", " + customer.getFirstname() + ")";
		
		return "success";
					
	}
	
	public String newCustomer() {
		
		customer = new CustomerView();
		username = "";
		password = "";

		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle rb = context.getApplication().getResourceBundle(context, "msg");	
		displayName = rb.getString("onlineferries_login");
		
		return "successNew";
	}

	public String saveCustomer() {
		
		Integer id = serviceFactory.getLoginService().saveCustomer(customer);
		customer.setCustomer_id(id);
		username = customer.getEmail();
		password = customer.getPassword();

		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle rb = context.getApplication().getResourceBundle(context, "msg");	
		displayName = rb.getString("onlineferries_logout") + "(" + customer.getLastname() + ", " + customer.getFirstname() + ")";
		
		return "successSave";
	}
	
	public String editCustomer() {
		
		serviceFactory.getLoginService().editCustomer(customer);
		username = customer.getEmail();
		password = customer.getPassword();

		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle rb = context.getApplication().getResourceBundle(context, "msg");	
		displayName = rb.getString("onlineferries_logout") + "(" + customer.getLastname() + ", " + customer.getFirstname() + ")";
		
		return "successEdit";
	}

	public String logout() {
		
		// Logiout ist auch eine Login (vom Text her)
		// funktioniert nicht grundsätzlich, bei successLogin erfolgt eine Weiterleitung zu login.xhtml und hier (momentan) zu 
		// showReservation. Da fehlt dann aber ggf. das Bereitstellen der anzuzeigenden Daten
		if (customer == null)
			return "successLogin";
		
		HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		
		if (session != null && customer != null) {
			System.out.println("lougout mit Session(isNew?): " + session.isNew());
			customer = null;
			username = null;
			password = null;

			FacesContext context = FacesContext.getCurrentInstance();
			ResourceBundle rb = context.getApplication().getResourceBundle(context, "msg");	
			displayName = rb.getString("onlineferries_login");
			
			session.invalidate();

		}
		System.out.println("logout ohne Session");
		return "successLogout";
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public CustomerView getCustomer() { return customer; }
	public void setCustomer(CustomerView customer) { this.customer = customer; }

	public void validateUsername(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
			
		String v = (String)value;
		if (v == null || v.length() < 3 || v.length() > 50) {
			ResourceBundle rb = context.getApplication().getResourceBundle(context, "msg");
			throw new ValidatorException(new FacesMessage(rb.getString("onlineferries_login_form_invalid_username")));

		}
	}

	public void validatePassword(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
			
		String v = (String)value;
		if (v == null || v.length() < 3 || v.length() > 50) {
			ResourceBundle rb = context.getApplication().getResourceBundle(context, "msg");
			throw new ValidatorException(new FacesMessage(rb.getString("onlineferries_login_form_invalid_password")));
		}
	}
	
	public void test() {
		System.out.println("TEST TEST TEST");
	}

}
