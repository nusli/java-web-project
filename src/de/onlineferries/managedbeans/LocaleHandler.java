package de.onlineferries.managedbeans;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@SessionScoped
public class LocaleHandler implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String locale;
		
	public String changeLocale() {
		Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		locale = params.get("locale");
		FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(locale));
		return null;
	}

	public String getLocale() {
		
		if (locale == null) {
			if (FacesContext.getCurrentInstance().getApplication().getDefaultLocale() != null)
				locale = FacesContext.getCurrentInstance().getApplication().getDefaultLocale().toString();
			else
				locale="de";
		}
		return locale;
	}
	
	public void setLocale(String locale) {
		this.locale = locale;
	}
}
