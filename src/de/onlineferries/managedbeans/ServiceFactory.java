package de.onlineferries.managedbeans;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.onlineferries.services.LoginService;
import de.onlineferries.services.ReservationService;
import de.onlineferries.services.RouteService;
import de.onlineferries.services.ShipService;

@Named
@ApplicationScoped
public class ServiceFactory implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private LoginService loginService;
	
	@Inject
	private RouteService routeService;
	
	@Inject
	private ShipService shipService;
	
	@Inject
	private ReservationService reservationService;
	
	public LoginService getLoginService() {
		return loginService;
	} 

	public RouteService getRouteService() {
		return routeService;
	}
	
	public ShipService getShipService() {
		return shipService;
	}
	
	public ReservationService getReservationService() {
		return reservationService;
	}
}
