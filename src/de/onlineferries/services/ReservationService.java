package de.onlineferries.services;

import java.util.List;

import de.onlineferries.view.ReservationTableView;
import de.onlineferries.view.ReservationView;
import de.onlineferries.view.ShipCabinView;

public interface ReservationService {

	public double getReservationPrice(int trip_id, List<ShipCabinView> shipCabins, int cars, int travellers);
	public void insertReservation(ReservationView  reservation);
	public List<ReservationTableView> getReservations(int customer_id);
	public void editReservation(ReservationView reservation);
	public ReservationView getReservation(int reservation_id);
}
