package de.onlineferries.services;

import de.onlineferries.view.CustomerView;

public interface LoginService {

	CustomerView login(String username, String password);
	public Integer saveCustomer(CustomerView customerView);
	public Integer editCustomer(CustomerView customerView);
}
