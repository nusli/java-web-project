package de.onlineferries.services;

import java.util.List;

import de.onlineferries.view.RouteView;
import de.onlineferries.view.TripView;

public interface RouteService {

	public List<RouteView> findAllRoutes();
	public List<TripView> findTrips(int route_id);
	public RouteView findRoute(int route_id);
}
