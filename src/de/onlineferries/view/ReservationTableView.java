package de.onlineferries.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.onlineferries.entities.CabinReservation;
import de.onlineferries.entities.Reservation;
import de.onlineferries.entities.Travellers;

public class ReservationTableView implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer reservation_id;
	private String routeStart, routeEnd;
	private String date, departure, arrival;
	private int cars;
	private List<CabinReservationView> cabinReservationViews;
	private List<TravellerView> travellerNames;

	public ReservationTableView(Integer reservation_id, String routeStart, String routeEnd, String date, String departure, String arrival, int cars,
			List<CabinReservationView> cabinReservationViews, List<TravellerView> travellerNames) {
		super();
		this.reservation_id = reservation_id;
		this.routeStart = routeStart;
		this.routeEnd = routeEnd;
		this.date = date;
		this.departure = departure;
		this.arrival = arrival;
		this.cars = cars;
		this.cabinReservationViews = cabinReservationViews;
		this.travellerNames = travellerNames;
	}

	public Integer getReservation_id() {
		return reservation_id;
	}

	public void setReservation_id(Integer reservation_id) {
		this.reservation_id = reservation_id;
	}

	public int getCars() {
		return cars;
	}

	public void setCars(int cars) {
		this.cars = cars;
	}

	public List<CabinReservationView> getCabinReservationViews() {
		return cabinReservationViews;
	}

	public void setCabinReservationViews(List<CabinReservationView> cabinReservationViews) {
		this.cabinReservationViews = cabinReservationViews;
	}

	public List<TravellerView> getTravellerNames() {
		return travellerNames;
	}

	public void setTravellerNames(List<TravellerView> travellerNames) {
		this.travellerNames = travellerNames;
	}

	public String getRouteEnd() {
		return routeEnd;
	}

	public void setRouteEnd(String routeEnd) {
		this.routeEnd = routeEnd;
	}

	public String getRouteStart() {
		return routeStart;
	}

	public void setRouteStart(String routeStart) {
		this.routeStart = routeStart;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public static ReservationTableView fromReservation(Reservation reservation) {
		List<CabinReservationView> cabins = new ArrayList<>();
		for(CabinReservation r : reservation.getCabins()) {
			cabins.add(CabinReservationView.fromCabinReservation(r));
		}
		List<TravellerView> travellers = new ArrayList<>();
		for(Travellers traveller : reservation.getTravellers()) {
			travellers.add(TravellerView.fromTraveller(traveller));
		}
		return new ReservationTableView(reservation.getId(), reservation.getTrip().getRoute().getStart(), 
				reservation.getTrip().getRoute().getDestination(), TripView.fromTrip(reservation.getTrip()).getFormatDate(), TripView.fromTrip(reservation.getTrip()).getFormatDeparture(), 
				TripView.fromTrip(reservation.getTrip()).getFormatArrival(), reservation.getCars(), cabins, travellers);
	}
		
}
