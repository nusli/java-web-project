package de.onlineferries.view;

import java.io.Serializable;
import de.onlineferries.entities.Customer;
 
public class CustomerView implements Serializable {

	private static final long serialVersionUID = 1L;

	private int customer_id;
	private String firstname;
	private String lastname;
	private String password;
	private String street;
	private String zip;
	private String city;
	private String email;
	private String iban;

	public CustomerView() {}
	
	public CustomerView(int customer_id, String email, String password, String lastname, String firstname) {
		super();
		this.customer_id = customer_id;
		this.email = email;
		this.firstname = firstname;
		this.lastname = lastname;
		this.password = password;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}
	
	public static CustomerView fromCustomer(Customer customer) {
		return new CustomerView(customer.getId(), customer.getEmail(), customer.getPassword(), customer.getName(), customer.getFirstname());
	}
}
