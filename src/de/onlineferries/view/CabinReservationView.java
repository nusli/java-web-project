package de.onlineferries.view;

import java.io.Serializable;
import java.util.List;

import de.onlineferries.entities.CabinReservation;

public class CabinReservationView implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String cabintype;
	private int count;
	
	public CabinReservationView(String cabintype, int count) {
		this.cabintype = cabintype;
		this.count = count;
	}

	public String getCabintype() {
		return cabintype;
	}

	public void setCabintype(String cabintype) {
		this.cabintype = cabintype;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public static CabinReservationView fromCabinReservation(CabinReservation cabinReservation) {
		return new CabinReservationView(cabinReservation.getCabin().getDescription(), cabinReservation.getCount());
	}
		
}
