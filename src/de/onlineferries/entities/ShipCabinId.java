package de.onlineferries.entities;

import java.io.Serializable;

public class ShipCabinId implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer ship;
	private Integer cabin;
	
	public Integer getShip() {
		return ship;
	}
	public void setShip(Integer ship_id) {
		this.ship = ship_id;
	}
	public Integer getCabin() {
		return cabin;
	}
	public void setCabin(Integer cabin_id) {
		this.cabin = cabin_id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cabin == null) ? 0 : cabin.hashCode());
		result = prime * result + ((ship == null) ? 0 : ship.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShipCabinId other = (ShipCabinId) obj;
		if (cabin == null) {
			if (other.cabin != null)
				return false;
		} else if (!cabin.equals(other.cabin))
			return false;
		if (ship == null) {
			if (other.ship != null)
				return false;
		} else if (!ship.equals(other.ship))
			return false;
		return true;
	}
		
}
