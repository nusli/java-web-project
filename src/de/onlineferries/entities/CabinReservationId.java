package de.onlineferries.entities;

import java.io.Serializable;

public class CabinReservationId implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer reservation;
	private Integer cabin;
	
	public Integer getReservation() {
		return reservation;
	}
	public void setReservation(Integer reservation_id) {
		this.reservation = reservation_id;
	}
	
	public Integer getCabin() {
		return cabin;
	}
	public void setCabin(Integer cabin_id) {
		this.cabin = cabin_id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cabin == null) ? 0 : cabin.hashCode());
		result = prime * result
				+ ((reservation == null) ? 0 : reservation.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CabinReservationId other = (CabinReservationId) obj;
		if (cabin == null) {
			if (other.cabin != null)
				return false;
		} else if (!cabin.equals(other.cabin))
			return false;
		if (reservation == null) {
			if (other.reservation != null)
				return false;
		} else if (!reservation.equals(other.reservation))
			return false;
		return true;
	}
	
}
